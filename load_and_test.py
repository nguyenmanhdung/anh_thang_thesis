import tensorflow as tf

labels = {0: "Cat", 1: "Dog"}

from tensorflow import keras
from tensorflow.keras import layers

def get_model():
    data_augmentation = keras.Sequential(
        [
            layers.RandomFlip("horizontal"),
            layers.RandomRotation(0.1),
        ]
    )

    base_model = keras.applications.Xception(
        weights="imagenet",  # Load weights pre-trained on ImageNet.
        input_shape=(150, 150, 3),
        include_top=False,
    )

    base_model.trainable = True

    inputs = keras.Input(shape=(150, 150, 3))
    x = data_augmentation(inputs)


    scale_layer = keras.layers.Rescaling(scale=1 / 127.5, offset=-1)
    x = scale_layer(x)

    x = base_model(x, training=False)
    x = keras.layers.GlobalAveragePooling2D()(x)
    x = keras.layers.Dropout(0.2)(x)  # Regularize with dropout
    outputs = keras.layers.Dense(1)(x)
    model = keras.Model(inputs, outputs)

    model.compile(
        optimizer=keras.optimizers.Adam(1e-5),
        loss=keras.losses.BinaryCrossentropy(from_logits=True),
        metrics=[keras.metrics.BinaryAccuracy()],
    )

    model.load_weights("saved_checkpoint/cp-0.9862424731254578.ckpt")

    return model

def decode_img(img):
  # Convert the compressed string to a 3D uint8 tensor
  img = tf.io.decode_jpeg(img, channels=3)
  # Resize the image to the desired size
  return tf.image.resize(img, [150, 150])

def process_path(file_path):
  # Load the raw data from the file as a string
  img = tf.io.read_file(file_path)
  img = decode_img(img)
  img = tf.expand_dims(img, 0)
  return img

model = get_model()
rs = model(process_path("/app/test_images/b25lY21zOmUzOTY3OGQ5LTU3NWItNGQyMC1iOTg2LWJjMWQxZjBhMjkyZToxN2E1ZDdlYS05YWVlLTRjMzMtOTBmMS1lNDFiOGJiOTUzMzM=.jpg"))
rs = tf.math.sigmoid(
    rs, name=None
)
rs = rs.numpy()
print(labels[round(rs.squeeze(0)[0])])